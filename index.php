<!doctype html>
<html lang="ru" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/site.min.css">
    <link rel="stylesheet" href="css/media.min.css">
    <script src="js/jquery.min.js"></script>
    <script src="js/jquery.maskedinput.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
    <script src="js/site.min.js"></script>
    <link rel="icon" href="favicon.ico" type="image/x-icon" />
    <title>Ликвидация ООО без вашего участия</title>
</head>
<body>
<section id="main">
    <header>
        <nav class="navbar navbar-default main__menu" role="navigation">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand logo__link menuLinks" href="index.php">
                    <span class="companyName"><img src="images/logo.png" alt="logo"></span><br>
                    <span class="companyService">Ликвидация ООО в Санкт-Петербурге</span>
                </a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav menu_right">
                    <li class="nav-item menu__item">
                        <a class="nav-link menuLinks" href="#main">Главная</a>
                    </li>
                    <li class="nav-item menu__item">
                        <a class="nav-link menuLinks" href="#services">Услуги</a>
                    </li>
                    <li class="nav-item menu__item">
                        <a class="nav-link menuLinks" href="#company">О компании</a>
                    </li>
                    <li class="nav-item menu__item">
                        <a class="nav-link menuLinks" href="#questions__form">Контакты</a>
                    </li>
                    <li class="nav-item menu__item menu__item_disable">
                        <a class="nav-link" href="tel: +78888888888">+7(888)888-88-88</a>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <div class="main__info">
        <h1 class="main__info_title">Официальная ликвидация ООО без вашего участия</h1>
        <h3 class="main__info_text">Сбросьте с себя груз ответственности за ненужное юридческое лицо и перестаньте терять деньги</h3>
        <button type="button" class="btn btn-success btn-consultation"
                data-toggle="modal" data-target="#formModal">Получить бесплатную консультацию</button>
        <p class="main__info_btn-text">Оставьте ваш номер телефона и в течении 15 минут вам позвонит
            внимательный менеджер и подробно проконсультирует
        </p>
    </div>
</section>
<section id="trust">
    <div class="container">
        <div class="row">
            <h2 class="trust_text">Посмотрите почему нам доверяют клиенты:</h2>
            <div class="col-md-12 trust__args">
                <div class="col-md-4 trust__advantage">
                    <div class="trust__number">1</div>
                    <div class="trust__title">Гарантия <br> результатов</div>
                    <div class="trust__text">Мы уверены в качестве наших услуг и вернем вам деньги в
                        случае ошибки с нашей стороны.
                    </div>
                </div>
                <div class="col-md-4 trust__advantage">
                    <div class="trust__number">2</div>
                    <div class="trust__title">Соблюдение <br> сроков</div>
                    <div class="trust__text">Выполняем работу максимально быстро и никогда не опаздываем.</div>
                </div>
                <div class="col-md-4 trust__advantage">
                    <div class="trust__number">3</div>
                    <div class="trust__title">Полное <br> сопровождение</div>
                    <div class="trust__text">Квалификационная помощь на всех этапах: от
                        консультаций до личного опытного ликвидатора
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="services">
    <div class="container">
        <div class="row">
            <h2 class="services_text">Выберите свой тариф</h2>
            <div class="services__tariffs">
                <div class="col-md-4 services__tariff">
                    <div class="tariff__info">
                        <div class="tariff__title">Премиум (?)</div>
                        <div class="tariff__price">29 700 р</div>
                        <div class="tariff__list">
                            <ul class="tariff__list">
                                <li>Личный менеджер</li>
                                <li>Любое количество консультаций</li>
                                <li>Подготовка полного пакета документов для ликвидации</li>
                                <li>Подготовка пакета документов для публикации в Вестнике</li>
                                <li>Подача/Получение документов</li>
                                <li>Публикация в Вестнике</li>
                            </ul>
                            <p>Все дополнительные расходы включены в стоимость:</p>
                            <ul class="tariff__list">
                                <li>Госпошлина</li>
                                <li>Нотариальные расходы</li>
                                <li>Публикация в Вестнике</li>
                            </ul>
                            <p>* Опытный ликвидатор от компании Профит</p>
                        </div>
                    </div>
                    <button type="button" class="btn btn-success tariff-btn" data-toggle="modal" data-target="#formModal">Подробнее</button>
                </div>
                <div class="col-md-4 services__tariff">
                    <div class="tariff__info services__tariff_border">
                        <div class="tariff__title">Оптимальный (?)</div>
                        <div class="tariff__price">14 700 р</div>
                        <div class="tariff__list">
                            <ul class="tariff__list">
                                <li>Подробная консультация</li>
                                <li>Подготовка полного пакета документов для ликвидации</li>
                                <li>Подготовка пакета документов для публикации в Вестнике</li>
                                <li>Подача/Получение документов</li>
                                <li>Публикация в Вестнике</li>
                            </ul>
                            <p>* Ликвидатор от заказчика</p>
                            <ul class="tariff__list"> Отдельно оплачиваются:
                                <li>Госпошлина 800р</li>
                                <li>Нотариальные расходы 6200р</li>
                                <li>Публикация в Вестнике 2000р</li>
                            </ul>
                        </div>
                    </div>
                    <button type="button" class="btn btn-success tariff-btn" data-toggle="modal" data-target="#formModal">Подробнее</button>
                </div>
                <div class="col-md-4 services__tariff">
                    <div class="tariff__info">
                        <div class="tariff__title">Минимум (?)</div>
                        <div class="tariff__price">4 900 р</div>
                        <div class="tariff__list">
                            <ul class="tariff__list">
                                <li>Подробная консультация</li>
                                <li>Подготовка полного пакета документов для ликвидации</li>
                                <li>Подготовка пакета документов для публикации в Вестнике(Без публикации)</li>
                            </ul>
                            <ul class="tariff__list"> Отдельно оплачиваются:
                                <li>Госпошлина 800р</li>
                                <li>Нотариальные расходы 6200р</li>
                                <li>Публикация в Вестнике 2000р</li>
                            </ul>
                        </div>
                    </div>
                    <button type="button" class="btn btn-success tariff-btn" data-toggle="modal" data-target="#formModal">Подробнее</button>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="company">
    <div class="container">
        <div class="row">
            <h2 class="company_text">Качество работы гарантирует</h2>
            <div class="company__block col-md-12">
                <div class="col-md-6 col-sm-6">
                    <img src="images/dir_photo.png" width="80%" alt="Директор Юлия" class="img-rounded">
                </div>
                <div class="col-md-6 company__director">
                    <h3>Юлия, Директор</h3>
                    <p>Здравствуйте, <br>
                        Занимаясь бухгалтерскими и юридическими услугами уже более 5 лет,
                        мы постоянно сталкиваемся с недостаточным пониманием людьми правовой
                        сферы. Ситуация осложняется тем фактом, что законодательство страны
                        и условия на рынке стремительно изменяются. Нужно постоянно развиваться
                        в это сфере, чтобы быть в курсе всех событий.
                    </p>
                    <p> Мы помогаем нашим клиентам быстро и просто шагнуть в бизнес среду.
                        На этом наше сотрудничество, конечно, не заканчивается. Мы с радостью
                        возьмем на себя любые задачи, от полного бухгалтерского сопровождения,
                        до единичного составления договора.
                    </p>
                    <p> Наша миссия - взять все ваши правовые задачи на себя, то бы вы не отвлекались
                        от того что вам действительно важно и интересно.
                    </p>
                </div>
            </div>
            <div class="company__block col-md-12">
                <div class="company__questions">
                    <h2>Нас часто спрашивают:</h2>
                    <div class="company__question">
                        <p class="company__question_title">1. Какие нужны документы для начала работы?</p>
                        <p class="company__question_text">Для начала работы необходимы учередительные документы
                        , паспортные данные и ИНН ликвидатора. Если ликвидатора назначем мы, то необходимы оригиналы документов.
                        Если ликвидатор ваш, то в оригиналах нет необходимости.</p>
                    </div>
                    <div class="company__question">
                        <p class="company__question_title">2. До какого момента необходимо сдавать отчеты при ликвидации?</p>
                        <p class="company__question_text">Отчетность сдается до момента подачи промежуточного ликвидационного баланса.
                        Вместе с балансом сдаются все необходимые отчеты за прошедший квартал.</p>
                    </div>
                    <div class="company__question">
                        <p class="company__question_title">3. В какой момент закрывать расчетный счет?</p>
                        <p class="company__question_text">Расчетный счет можно закрыть в любой момент до подачи промежуточного
                        ликвидационного баланса.</p>
                    </div>
                    <div class="company__question">
                        <p class="company__question_title">4. Как долго длится ликвидация? Чем обусловлено?</p>
                        <p class="company__question_text">Срок ликвидации примерно 3 месяца, так как обязательным условием
                        является публикация в специализированных журналах и небходимо ждать 2 месяца прежде чем подавать
                        окончательные документы на ликвидацию. Помимо этого в налоговую подается несколько форм и их
                        обработка тоже занимает время.</p>
                    </div>
                    <div class="company__question">
                        <p class="company__question_title">5. Можете ли гарантировать что не будет проверки?</p>
                        <p class="company__question_text">Гарантировать отсутствие проверки мы не можем, но если у вас фирма
                        нулевая или с небольшими оборотами, то вероятность проверки сводится к минимуму.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="questions__form">
    <div class="container">
        <div class="row">
            <h2>Остались вопросы?</h2>
            <div class="question__form_block">
                <p class="question__form_title">
                    Заполните форму ниже и в течении 15 минут с вами свяжется внимательный менеджер и подробно
                    проконсультирует
                </p>
                <form class="form-horizontal question__form">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <input class="form-control" name="name" placeholder="Имя">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <input class="form-control formPhone" name="phone" placeholder="Телефон">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <button type="button" class="btn btn-success btn-block btn-consultation_form"
                                    onclick="sendForm(this);">Получить бесплатную консультацию</button>
                        </div>
                    </div>
                </form>
                <p class="question__form_note">Нажимая на кнопку вы подтверждаете согласие на обработку персональных данных</p>
            </div>
        </div>
    </div>
</section>
<section id="company__map">
    <div id="map"></div>
</section>
<section id="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-3 footer__logo">
                <a class="logo__link menuLinks" href="#main">
                    <span class="companyName"><img src="images/logo.png" alt="logo"></span><br>
                    <span class="companyService">Ликвидация ООО в Санкт-Петербурге</span>
                </a>
            </div>
            <div class="col-md-6 social__block">
                <p data-toggle="modal" data-target="#secureModal">Политика конфиденциальности</p>
            </div>
            <div class="col-md-3">
                <a class="contactTel" href="tel: +78888888888">+7(888)888-88-88</a>
                <p class="contactAddress">Адрес: г. Санкт-Петербург, проспект
                    Просвещения, д.33 корп 1, 2 этаж
                </p>
            </div>
        </div>
    </div>
</section>


<!-- Modal Form-->
<div class="modal fade" id="formModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <div class="question__form_block fullWidth">
                    <p class="question__form_title">
                        Заполните форму ниже и в течении 15 минут с вами свяжется внимательный менеджер и подробно
                        проконсультирует
                    </p>
                    <form class="form-horizontal question__form">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <input class="form-control" name="name" placeholder="Имя">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <input class="form-control formPhone" name="phone" placeholder="Телефон">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <button type="button" class="btn btn-success btn-block btn-consultation_form"
                                        onclick="sendForm(this);">Получить бесплатную консультацию</button>
                            </div>
                        </div>
                    </form>
                    <p class="question__form_note">Нажимая на кнопку вы подтверждаете согласие на обработку персональных данных</p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="secureModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <div class="fullWidth">
                    <h2>Политика конфиденциальности</h2>
                    <p>
                        Соглашение об обработке персональных данных. Данное соглашение об обработке персональных данных
                        разработано в соответствии с законодательством Российской Федерации. Все лица заполнившие сведения,
                        составляющие персональные данные на данном сайте, а также разместившие иную информацию обозначенными
                        действиями подтверждают свое согласие на обработку персональных данных и их передачу оператору
                        обработки персональных данных. Под персональными данными Гражданина понимается ниже указанная
                        информация: Общая информация (Имя, адрес электронной почты, телефон); посетители сайта направляют
                        свои персональные данные для получения доступа к информации предлагаемой на сайте. Гражданин,
                        принимая настоящее Соглашение, выражает свою заинтересованность и полное согласие, что обработка
                        их персональных данных может включать в себя следующие действия: сбор, систематизацию, накопление,
                        хранение, уточнение (обновление, изменение), использование, информирование посредствам звонка,
                        смс, e-mail; Гражданин гарантирует: информация, им предоставленная, является полной, точной и
                        достоверной; при предоставлении информации не нарушается действующее законодательство
                        Российской Федерации, законные права и интересы третьих лиц; вся предоставленная информация
                        заполнена Гражданина в отношении себя лично.
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>