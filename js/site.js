$(document).ready(function () {
    $('.formPhone').mask("+7(999)999-9999");
    $("a.menuLinks").click(function () {
        var elementClick = $(this).attr("href");
        var destination = $(elementClick).offset().top;
        destination -= 53;
        $('html,body').stop().animate({scrollTop: destination}, 1100);
        return false;
    });
    ymaps.ready(init);
    var myMap,
        myPlacemark;

    function init() {
        myMap = new ymaps.Map("map", {
            center: [60.050017, 30.345818],
            controls: ['zoomControl'],
            zoom: 13
        });

        myPlacemark = new ymaps.Placemark([60.050017, 30.345818], {
            hintContent: 'г. Санкт-Петербург',
            balloonContent: 'г. Санкт-Петербург, проспект Просвещения, д.33 корп 1, 2 этаж'
        });

        myMap.geoObjects.add(myPlacemark);
    }
});
$(window).on('scroll', function () {
    if ($(window).scrollTop() >= 53) {
        $('header').css({
            position: 'fixed',
            background: "#fff",
            zIndex: 5
        });
    } else {
        $('header').css({
            position: '',
            background: "",
            zIndex: ''
        });
    }
});

/**
 * Обработка формы с Консультацией
 * @param obj Кнопка отправки формы
 */
function sendForm(obj) {
    var name = $(obj).closest('.question__form').find('input[name="name"]');
    var phone = $(obj).closest('.question__form').find('input[name="phone"]');
    if ($.trim(name.val()) == '') {
        name.closest('.form-group').removeClass('has-success');
        name.closest('.form-group').addClass('has-error');
    } else {
        name.closest('.form-group').removeClass('has-error');
        name.closest('.form-group').addClass('has-success');
    }
    if ($.trim(phone.val()) == '') {
        phone.closest('.form-group').removeClass('has-success');
        phone.closest('.form-group').addClass('has-error');
    } else {
        phone.closest('.form-group').removeClass('has-error');
        phone.closest('.form-group').addClass('has-success');
    }
    if ($.trim(name.val()) != '' && $.trim(phone.val()) != '') {
        $.ajax({
            type: 'POST',
            url: 'form.php',
            data: $(obj).closest('.question__form').serializeArray(),
            success: function(msg){
                if (msg == 'true') {
                    $('.question__form_block').html('');
                    $('.question__form_block').append('<div class="success__block">'+
                        '<div class="block_img">'+
                            '<img src="./images/success.png" class="success__block__img img-rounded" alt="success">'+
                        '</div>'+
                            '<p class="success__block__text">Спасибо, ваш запрос принят.<br> ' +
                                'Ожидайте, наш консультант свяжется с Вами в ближайшее время.</p>'+
                    '</div>');
                }
                else {
                    $('.question__form_block').html('');
                    $('.question__form_block').append('<p class="error__block__text">К сожалению не удалось отправить Вашу заявку.<br> ' +
                        'Пожалуйста, повторите попытку позднее.</p>'+
                        '</div>');
                }
            }
        })
    }
}
